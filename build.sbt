name := "ConditionalRead"

version := "0.1"

scalaVersion := "2.11.8"

libraryDependencies += "com.typesafe.play" %% "play-json" % "2.5.4"

libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.6"
