package playJsonTools

import org.scalatest.{MustMatchers, WordSpec}
import play.api.data.validation.ValidationError
import play.api.libs.functional.syntax._
import play.api.libs.json.Json._
import play.api.libs.json._
import playJsonTools.JsErrorHelper._
import playJsonTools.JsPathImplicits._

case class WithPass(server: String, port: Int, username: String, password: String)
case class WithKey(server: String, port: Int, username: String, key: String)


class ConditionalReadsSpec extends WordSpec with MustMatchers {

  "ConditionalReads" must {

    sealed trait IntOrString
    case class CoInt(n: Int) extends IntOrString
    case class CoString(str: String) extends IntOrString

    implicit val coIntReads: Reads[CoInt] = Reads {
      case JsNumber(n) => JsSuccess(CoInt(n.toInt))
      case _ => JsError("unexpected error")
    }

    implicit val coStringReads: Reads[CoString] = Reads {
      case JsString(str) => JsSuccess(CoString(str))
      case _ => JsError("unexpected error")
    }

    val json = Json.obj(
      "enabled" -> true,
      "int" -> 1,
      "string" -> "foo"
    )
    val jsonNull = Json.obj(
      "int" -> JsNull,
      "string" -> JsNull
    )

    "read a matched condition" in {
      val reader =(__ \ "int").readIfHeadExist[Int]
      (__ \ "int").readIfHeadExist[Int].reads(json) mustBe JsSuccess(1, stringToJsPath("int"))
    }

    "compose when one 'if' matched" in {
      val reader =
        (__ \ "int").readIfHeadExist[Int] xor
          (__ \ "otherInt").readIfHeadExist[Int]

      reader.reads(json) mustBe JsSuccess(1, stringToJsPath("int"))

      val reader2 = IfExist("enabled", JsBoolean(true)).thenRead[String]((__ \ "string").read[String])
      reader2.reads(json) mustBe JsSuccess("foo", stringToJsPath("string"))
    }

    "compose when several 'if' matched" in {
      val reader =
        (__ \ "int").readIfHeadExist[CoInt].ofType[IntOrString] xor
          (__ \ "string").readIfHeadExist[CoString]

      reader.reads(json) mustBe JsError(Seq(
        (JsPath(List.empty), Seq(
          ValidationError(JsErrorHelper.expectedMaximumOne, JsErrorHelper.formatItems(Seq("int", "string")))
        )
          )))
    }

    "compose when an if matched but then failed" in {
      val reader =
        (__ \ "int").readIfHeadExist[String] xor
          (__ \ "foo").readIfHeadExist[String]

      reader.reads(json) mustBe JsError(Seq(
        ((__ \ "int"), Seq(ValidationError("error.expected.jsstring")))
      ))
    }


    "missing path error aggregation if no match" in {
      val reader =
        (__ \ "foo").readIfHeadExist[Int] xor
          (__ \ "bar").readIfHeadExist[Int] xor
          (__ \ "string").readsIf[Int](_.read[Int])

      reader.reads(json) mustBe JsError(Seq(
        (JsPath(List.empty), Seq(ValidationError(expectedKeysErrorCode, formatItems(List("bar", "foo"))))),
        (JsPath(List(KeyPathNode("string"))), Seq(ValidationError("error.expected.jsnumber"))
          )))
    }



    "recoverFailedConditionWith idendity" in {
      val reader = (__ \ "foo").readIfHeadExist[Int].recoverFailedConditionWith(ConditionalReads.failedConditionHandlerIdentity)

      reader.reads(json) mustBe JsError(ValidationError(expectedKeysErrorCode, formatItems(List("foo"))))
    }

    "recoverFailedConditionWith with valid recovering" in {
      val reader = (__ \ "foo").readIfHeadExist[Int].recoverFailedConditionWith(_ => (__ \ "int").read[Int])

      reader.reads(json) mustBe JsSuccess(1, JsPath(List(KeyPathNode("int"))))
    }

    "read FTPConnector" in {

      trait Connector
      case class FTPInputConnector(server: String,
                                   port: Int,
                                   username: String,
                                   credential: Either[String, String]) extends Connector
      case object InactiveConnector extends Connector




      implicit val ftpRead = new Reads[FTPInputConnector]() {
        override def reads(json: JsValue): JsResult[FTPInputConnector] = {
          (
            ((json \ "server").validate[String] and
              (json \ "port").validate[Int] and
              (json \ "username").validate[String] and
              (json \ "password").validateOpt[String] and
              (json \ "key").validateOpt[String]
              ) ((server, port, username, password, key) => (server, port, username, password, key))
            ).flatMap{
            case (server, port, username, Some(pass:String), _) => JsSuccess(FTPInputConnector(server, port, username, Left(pass)))
            case (server, port, username, None, Some(key:String)) => JsSuccess(FTPInputConnector(server, port, username, Right(key)))
            case (server, port, username, None, None) => JsError("FTPInputConnector need a password or a key")
          }
        }
      }

      val ftpRead2 : Reads[FTPInputConnector] = {
        Json.reads[WithPass].map{ x => FTPInputConnector(x.server, x.port, x.username, Left(x.password))} |
          Json.reads[WithKey].map{ x => FTPInputConnector(x.server, x.port, x.username, Right(x.key))}
      }


      val connectorReads = (__ \ "ftp").readIfHeadExist[FTPInputConnector].ofType[Connector] xor
        (__ \ "inactive").readIfHeadExist(Reads.pure(InactiveConnector))


      val ftp = Json.obj(
        "server" -> "s",
        "port" -> 1,
        "username" -> "foo",
        "password" -> "baar"
      )
      val ic = Json.obj( "inactive" -> "randomSTuff")
      val ftpc = Json.obj("ftp" -> ftp)
      ftpRead2.reads(ftp) mustBe JsSuccess(FTPInputConnector("s",1,"foo",Left("baar")))
      connectorReads.reads(ic) mustBe JsSuccess(InactiveConnector, stringToJsPath("inactive"))
      connectorReads.reads(ftpc) mustBe JsSuccess(FTPInputConnector("s",1,"foo",Left("baar")), stringToJsPath("ftp"))
    }
  }
}
