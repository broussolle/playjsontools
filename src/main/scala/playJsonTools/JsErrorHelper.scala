package playJsonTools

import play.api.data.validation.ValidationError
import play.api.libs.json.{JsError, JsPath, KeyPathNode}

object JsErrorHelper {

  implicit def stringToJsPath(path: String) = JsPath(List(KeyPathNode(path)))

  def pathToString(path: JsPath): String = {
    val strPath = path.toString
    if (strPath.startsWith("/")) strPath.tail else strPath
  }

  val expectedKeysErrorCode = "validate.error.expected.missing"
  val expectedMaximumOne = "validate.error.expected.maximum.one"
  def expectedXInsteadOf(good: String, wrong:String) = s"validate.error.expected.$good.instead.of.$wrong"


  def formatItems(items: Seq[Any]): String = items.map(key => "\"" + key + "\"").mkString(", ")

  def aggregateMissingKeys(err: JsError): JsError = {
    val (missingPathErrors, otherErrors) = err.errors.partition {
      case (_, validationErrors) => validationErrors.exists(_.message == "error.path.missing")
    }

    val missingPaths = missingPathErrors
      .map { case (path, _) => pathToString(path) }
      .distinct

    val aggregateMissingPathErrors = if (missingPaths.isEmpty)
        Seq()
      else
        Seq((JsPath(List.empty), Seq(ValidationError(expectedKeysErrorCode, formatItems(missingPaths)))))

    JsError(aggregateMissingPathErrors ++ otherErrors)
  }

}
