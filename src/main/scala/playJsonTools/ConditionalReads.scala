package playJsonTools

import JsErrorHelper._
import JsErrorHelper.expectedMaximumOne
import play.api.data.validation.ValidationError
import play.api.libs.json._

/**
  * A ConditionalReads is composed of a conditionalReads (type Reads[_]) and a thenReads(type Reads[A]).
  * When a ConditionalReads is applied on a Json (.reads(json)), it first applies the conditionReads on the Json:
  *   - if successful, it apply the thenReads on the Json and returns the resultant JsResult.
  *   - if error, it returns the JsError of the conditionalReads.
  *
  * A common use case is for parsing Sum type (A | B | C) of common parent type X (ex: with a sealed trait):
  * val reader: ConditionalReads[X] =
  *   (__ \ "a").readsIfPathExist[A].ofType[X] xor
  *   (__ \ "b").readsIfPathExist[B] xor
  *   (__ \ "c").readsIfPathExist[C]
  *
  * In this reader, if none of the paths are found, the resultant JsError of reader.reads(json) will be the accumulation of the condition errors,
  * thus showing all the possible paths that were possible.
  */
trait ConditionalReads[A] extends Reads[A] {
  self =>

  val conditionsAndThen: Seq[(Reads[_], Reads[A])]
  val failedConditionHandler: JsError => Reads[A]

  override def reads(json: JsValue): JsResult[A] = {
    val conditionsAndThenResults: Seq[(JsResult[_], Option[JsResult[A]])] = conditionsAndThen.map { case (condRead, thenRead) =>
      condRead.reads(json) match {
        case success@JsSuccess(_, _) => (success, Some(thenRead.reads(json)))
        case error: JsError => (error, None)
      }
    }

    val successfulConditions = conditionsAndThenResults.collect {
      case (success@JsSuccess(_, _), Some(thenResult)) => (success, thenResult)
    }

    successfulConditions.length match {
      case 0 =>
        val error = conditionsAndThenResults
          .map(_._1.asInstanceOf[JsError]) // safe (otherwise algorithmic error)
          .reduceLeft(JsError.merge)

        failedConditionHandler(error).reads(json)

      case 1 =>
        conditionsAndThenResults
          .collect { case (_, Some(thenResult)) => thenResult }
          .head // safe (otherwise algorithmic error)

      case _ =>
        val successfulPaths = successfulConditions.map(_._1.path)
        val formattedKeys = JsErrorHelper.formatItems(successfulPaths.map(JsErrorHelper.pathToString))
        val error = JsError(ValidationError(expectedMaximumOne, formattedKeys))
        error
    }
  }

  def xor[B <: A](conditionRead: ConditionalReads[B]): ConditionalReads[A] = {
    new ConditionalReads[A] {
      override val conditionsAndThen: Seq[(Reads[_], Reads[A])] = self.conditionsAndThen ++ conditionRead.map(b => b: A).conditionsAndThen
      override val failedConditionHandler = self.failedConditionHandler
    }
  }

  override def map[B](f: A => B): ConditionalReads[B] = {
    new ConditionalReads[B] {
      override val conditionsAndThen = self.conditionsAndThen.map { case (ifCond, thenRead) => (ifCond, thenRead.map(f)) }
      override val failedConditionHandler = self.failedConditionHandler andThen (readsA => readsA.map(f))
    }
  }

  def recoverFailedConditionWith[B <: A](f: JsError => Reads[B]): ConditionalReads[A] = {
    new ConditionalReads[A] {
      override val conditionsAndThen = self.conditionsAndThen
      override val failedConditionHandler: JsError => Reads[A] = error => {
        Reads[A] { json =>
          self.failedConditionHandler(error).reads(json) match {
            case success @ JsSuccess(_, _) => success
            case error: JsError => f(error).reads(json).map(b => b: A)
          }
        }
      }
    }
  }

  def ofType[B >: A]: ConditionalReads[B] = map(a => a: B)
}

object ConditionalReads {
  def failedConditionHandlerIdentity[A]: JsError => Reads[A] = error => Reads.apply(_ => error)

  def pathExists(path: JsPath) = path.read[JsValue]

  def ifExist(path: JsPath, expected: JsValue) = Reads { json =>
    path.read[JsValue].reads(json) match {
      case JsSuccess(x, _) if x == expected => JsSuccess(())
      case js => JsError(ValidationError(expectedXInsteadOf(expected.toString(), js.toString()), path))
    }
  }

  def apply[A](condition: Reads[_],
               thenReads: Reads[A],
               failedConditionMapper: JsError => JsError = JsErrorHelper.aggregateMissingKeys) = {
    new ConditionalReads[A] {
      override val conditionsAndThen = Vector((condition, thenReads))
      override val failedConditionHandler: JsError => Reads[A] = error => Reads.apply(_ => failedConditionMapper(error))
    }
  }
}

case class   IfExist(path: JsPath, expected: JsValue) {
  def thenRead[A](implicit thenReads: Reads[A]) = {
    val condition = Reads { json =>
      path.read[JsValue].reads(json) match {
        case JsSuccess(x, _) if x == expected => JsSuccess(())
        case js => JsError(ValidationError(expectedXInsteadOf(expected.toString(), js.toString()), path))
      }
    }
    ConditionalReads(condition, thenReads, error => error)
  }
}

object JsPathImplicits {
  implicit class RichJsPath(val path: JsPath) extends AnyVal {
    def readsIf[B](condition: JsPath => Reads[_])(implicit thenRead: Reads[B]): ConditionalReads[B] =
      ConditionalReads(condition(path), Reads.at(path)(thenRead))

    def readIfHeadExist[B](implicit thenRead: Reads[B]): ConditionalReads[B] = readsIf(ConditionalReads.pathExists)
  }
}
